<?php
class Aws_Study_Block_Banner extends  Mage_Core_Block_Template
{
    public function getRecentProducts() {
        // call model to fetch data
        $arr_products = array();
        $products = Mage::getModel("aws_study/banner")->getRecentProducts();

    foreach ($products as $product) {
        $arr_products[] = array(
            'id' => $product->getId(),
            'name' => $product->getName(),
            'url' => $product->getProductUrl(),
        );
    }

    return $arr_products;
  }
}
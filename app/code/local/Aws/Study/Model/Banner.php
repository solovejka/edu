<?php

class Aws_Study_Model_Banner extends Mage_Core_Model_Abstract
{
    public function getRecentProducts() {
        $products = Mage::getModel("catalog/product")
                ->getCollection()
                ->addAttributeToSelect('*')
                ->setOrder('entity_id', 'DESC')
                ->setPageSize(5);
    return $products;
  }

}